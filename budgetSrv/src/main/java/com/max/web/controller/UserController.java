package com.max.web.controller;

import com.max.converter.UserConverter;
import com.max.data.domain.User;
import com.max.services.UserService;
import com.max.web.dto.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    private final UserConverter converter;

    public UserController(UserService userService, UserConverter converter) {
        this.userService = userService;
        this.converter = converter;
    }

    @GetMapping("/")
    public ResponseEntity<?> getAllUsers() {
        List<UserDto> users = userService.findAll().stream()
                .map(converter::convertDatatoDto)
                .collect(Collectors.toList());
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<?> getUser(@PathVariable Long userId) {
        return new ResponseEntity<>(userService.findById(userId), HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> createUser(@RequestBody UserDto userDto) {
        User usr = converter.convertDtoToData(userDto);
        userService.createUser(usr);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @DeleteMapping("/")
    public ResponseEntity<?> deleteAllUsers() {
        userService.deleteAllUsers();
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable Long userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

}

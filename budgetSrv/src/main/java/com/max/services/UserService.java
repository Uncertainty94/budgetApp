package com.max.services;

import com.max.data.domain.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    User findById(Long id);

    void createUser(User user);

    void deleteAllUsers();

    void deleteUser(Long id);
}

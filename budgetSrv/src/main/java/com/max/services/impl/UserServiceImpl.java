package com.max.services.impl;

import com.max.data.domain.User;
import com.max.data.repository.UserRepository;
import com.max.services.UserService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    public List<User> findAll() {
        return (List<User>) repository.findAll();
    }

    public User findById(Long id) {
        return repository.findById(id).get();
    }

    public void createUser(User user) {
        user.setCreatedWhen(new Date());
        repository.save(user);
    }

    public void deleteAllUsers() {
        repository.deleteAll();
    }

    public void deleteUser(Long id) {
        repository.deleteById(id);
    }
}

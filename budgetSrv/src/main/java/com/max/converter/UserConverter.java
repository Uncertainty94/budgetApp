package com.max.converter;

import com.max.data.domain.User;
import com.max.web.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public class UserConverter {

    public User convertDtoToData(UserDto dto) {
        User user = new User();
        user.setId(dto.getId());
        user.setName(dto.getName());
        user.setPass(dto.getPass());
        return user;
    }

    public UserDto convertDatatoDto(User user) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setName(user.getName());
        dto.setPass(user.getPass());
        return dto;
    }
}
